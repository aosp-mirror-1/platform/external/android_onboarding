package com.android.onboarding.bedsteadonboarding.utils

/** Utility class for logging of events. */
object EventLogUtils {

  /** Onboarding event log tag */
  const val ONBOARDING_EVENT_LOG_TAG = "ZZOnboardingGraph"

}
