package com.android.onboarding.contracts.annotations

/** Represents the metadata extracted from processing [OnboardingNode] annotation. */
data class OnboardingNodeMetadata(val packageName: String, val component: String)
